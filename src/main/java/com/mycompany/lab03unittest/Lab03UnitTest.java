/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 */

package com.mycompany.lab03unittest;

import java.util.Scanner;

public class Lab03UnitTest {
    static char[][] board;
    static char CurrenPlayer = 'X';
    
    static void CreateBoard() {
        for (int i = 0; i < 3; i++) {
            for (int j = 0; j < 3; j++) {
                board[i][j] = '-';
            }
        }
    }
    static void printBoard() {
        System.out.println("-------------");
        for (int i = 0; i < 3; i++) {
            System.out.print("| ");
            for (int j = 0; j < 3; j++) {
                System.out.print(board[i][j] + " | ");
            }
            System.out.println();
            System.out.println("-------------");
        }
    }
    static void switchPlayer() {
        CurrenPlayer = (CurrenPlayer == 'X') ? 'O' : 'X';
    }
    static boolean makeXO(int position) {
        int row = (position - 1) / 3;
        int col = (position - 1) % 3;
        if (position >= 1 && position <= 9 && board[row][col] == '-') {
            board[row][col] = CurrenPlayer;
            return false;
        }
        return true;
    }
    static boolean BoardFull(char[][] board) {
        for (int i = 0; i < 3; i++) {
            for (int j = 0; j < 3; j++) {
                if (board[i][j] == '-') {
                    return false;
                }
            }
        }
        return true;
    }
    static boolean checkWin(char[][] board,char CurrenPlayer) {
        return checkRowWin(board, CurrenPlayer) || checkColWin(board, CurrenPlayer) || checkDiagonalWin(board, CurrenPlayer);
    }
    static boolean checkRowWin(char[][] board,char CurrenPlayer) {
        for (int i = 0; i < 3; i++) {
            if (board[i][0] != '-' && board[i][0] == board[i][1] && board[i][1] == board[i][2] && board[i][0] == CurrenPlayer) {
                return true;
            }
        }
        return false;
    }
    static boolean checkColWin(char[][] board,char CurrenPlayer) {
        for (int i = 0; i < 3; i++) {
            if (board[0][i] != '-' && board[0][i] == board[1][i] && board[1][i] == board[2][i] && board[0][i] == CurrenPlayer) {
                return true;
            }
        }
        return false;
    }
    static boolean checkDiagonalWin(char[][] board,char CurrenPlayer) {
        return (board[0][0] != '-' && board[0][0] == board[1][1] && board[1][1] == board[2][2] && board[0][0] == CurrenPlayer) ||
                (board[0][2] != '-' && board[0][2] == board[1][1] && board[1][1] == board[2][0] && board[0][2] == CurrenPlayer);
    }
    static boolean checkDraw(char[][] board, char CurrenPlayer) {
        if(checkWin(board, CurrenPlayer) == true) {
            return false;
        }return true;
 // Generated from nbfs://nbhost/SystemFileSystem/Templates/Classes/Code/GeneratedMethodBody
    }
    static void GameSet() {
        board = new char[3][3];
        CurrenPlayer = 'X';
        CreateBoard();
    }

    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        boolean continueGame = false;
        System.out.println("Welcome to XO!");
        System.out.println("Player 1: X");
        System.out.println("Player 2: O");
        System.out.println("Let's start the game!\n");
        do{
            GameSet();
            do{
                printBoard();
                int position;
                do{
                    System.out.print("Player " + CurrenPlayer + ", enter your move (1-9): ");
                    position = sc.nextInt();
                } while (makeXO(position));

                if (checkWin(board, CurrenPlayer)){
                    System.out.println("Player " + CurrenPlayer + " wins!");
                    break;
                } else if (BoardFull(board)){
                    System.out.println("It's a draw!");
                    break;
                } else if (checkDraw(board, CurrenPlayer)){
                    System.out.println("It's a draw!");
                    break;
                }
                switchPlayer();
            }while(!checkWin(board, CurrenPlayer)&&!BoardFull(board));
            printBoard();
            boolean roop = false;
            do{
                System.out.print("Do you want to continue playing? (Y/N): ");
                String input = sc.next();
                char character = input.charAt(0);
                if(character == 'Y' || character == 'y'){
                    roop = false;
                    continueGame = true;
                }else if(character == 'N' || character == 'n'){
                    roop = false;
                    continueGame = false;
                }else{
                    roop = true;
                }
            }while(roop);
        }while(continueGame);
    }

    
}
