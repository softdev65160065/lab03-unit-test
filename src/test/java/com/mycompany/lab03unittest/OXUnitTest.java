/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/UnitTests/JUnit5TestClass.java to edit this template
 */
package com.mycompany.lab03unittest;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;

/**
 *
 * @author My Computer
 */
public class OXUnitTest {
    
    public OXUnitTest() {
    }

    @org.junit.jupiter.api.BeforeAll
    public static void setUpClass() throws Exception {
    }

    @org.junit.jupiter.api.AfterAll
    public static void tearDownClass() throws Exception {
    }

    @org.junit.jupiter.api.BeforeEach
    public void setUp() throws Exception {
    }

    @org.junit.jupiter.api.AfterEach
    public void tearDown() throws Exception {
    }


    // TODO add test methods here.
    // The methods must be annotated with annotation @Test. For example:
    //
    // @Test
    // public void hello() {}
    @Test
    public void testCheckWin_O_row1_output_ture() {
        char[][] board = {{'O','O','O'},{'-','-','-'},{'-','-','-'}};
        char CurrenPlayer = 'O';
        boolean result = Lab03UnitTest.checkWin(board, CurrenPlayer);
        assertEquals(true, result);
    }
    @Test
    public void testCheckWin_O_row2_output_true() {
        char[][] board = {{'-','-','-'},{'O','O','O'},{'-','-','-'}};
        char CurrenPlayer = 'O';
        boolean result = Lab03UnitTest.checkWin(board, CurrenPlayer);
        assertEquals(true, result);
    }
    @Test
    public void testCheckWin_O_row3_output_true() {
        char[][] board = {{'-','-','-'},{'-','-','-'},{'O','O','O'}};
        char CurrenPlayer = 'O';
        boolean result = Lab03UnitTest.checkWin(board, CurrenPlayer);
        assertEquals(true, result);
    }
    @Test
    public void testCheckWin_X_col1_output_true() {
        char[][] board = {{'X','-','-'},{'X','-','-'},{'X','-','-'}};
        char CurrenPlayer = 'X';
        boolean result = Lab03UnitTest.checkWin(board, CurrenPlayer);
        assertEquals(true, result);
    }
    @Test
    public void testCheckWin_X_col2_output_true() {
        char[][] board = {{'-','X','-'},{'-','X','-'},{'-','X','-'}};
        char CurrenPlayer = 'X';
        boolean result = Lab03UnitTest.checkWin(board, CurrenPlayer);
        assertEquals(true, result);
    }
    @Test
    public void testCheckWin_X_col3_output_true() {
        char[][] board = {{'-','X','-'},{'-','X','-'},{'-','X','-'}};
        char CurrenPlayer = 'X';
        boolean result = Lab03UnitTest.checkWin(board, CurrenPlayer);
        assertEquals(true, result);
    }
    @Test
    public void testCheckWin_O_col1_output_true() {
        char[][] board = {{'O','-','-'},{'O','-','-'},{'O','-','-'}};
        char CurrenPlayer = 'O';
        boolean result = Lab03UnitTest.checkWin(board, CurrenPlayer);
        assertEquals(true, result);
    }
    @Test
    public void testCheckWin_O_col2_output_true() {
        char[][] board = {{'-','O','-'},{'-','O','-'},{'-','O','-'}};
        char CurrenPlayer = 'O';
        boolean result = Lab03UnitTest.checkWin(board, CurrenPlayer);
        assertEquals(true, result);
    }
    @Test
    public void testCheckWin_O_col3_output_true() {
        char[][] board = {{'-','O','-'},{'-','O','-'},{'-','O','-'}};
        char CurrenPlayer = 'O';
        boolean result = Lab03UnitTest.checkWin(board, CurrenPlayer);
        assertEquals(true, result);
    }
    @Test
    public void testCheckWin_X_row1_output_ture() {
        char[][] board = {{'X','X','X'},{'-','-','-'},{'-','-','-'}};
        char CurrenPlayer = 'X';
        boolean result = Lab03UnitTest.checkWin(board, CurrenPlayer);
        assertEquals(true, result);
    }
    @Test
    public void testCheckWin_X_row2_output_true() {
        char[][] board = {{'-','-','-'},{'X','X','X'},{'-','-','-'}};
        char CurrenPlayer = 'X';
        boolean result = Lab03UnitTest.checkWin(board, CurrenPlayer);
        assertEquals(true, result);
    }
    @Test
    public void testCheckWin_X_row3_output_true() {
        char[][] board = {{'-','-','-'},{'-','-','-'},{'X','X','X'}};
        char CurrenPlayer = 'X';
        boolean result = Lab03UnitTest.checkWin(board, CurrenPlayer);
        assertEquals(true, result);
    }
    @Test
    public void testCheckWin_O_Digonal1_output_true() {
        char[][] board = {{'O', '-', '-'}, {'-', 'O', '-'}, {'-', '-', 'O'}};
        char CurrenPlayer = 'O';
        boolean result = Lab03UnitTest.checkWin(board, CurrenPlayer);
        assertEquals(true, result);
    }
    @Test
    public void testCheckWin_O_Digonal2_output_true() {
        char[][] board = {{'-', '-', 'O'}, {'-', 'O', '-'}, {'O', '-', '-'}};
        char CurrenPlayer = 'O';
        boolean result = Lab03UnitTest.checkWin(board, CurrenPlayer);
        assertEquals(true, result);
    }
    @Test
    public void testCheckWin_X_Digonal1_output_true() {
        char[][] board = {{'X', '-', '-'}, {'-', 'X', '-'}, {'-', '-', 'X'}};
        char CurrenPlayer = 'X';
        boolean result = Lab03UnitTest.checkWin(board, CurrenPlayer);
        assertEquals(true, result);
    }
    @Test
    public void testCheckWin_X_Digonal2_output_true() {
        char[][] board = {{'-', '-', 'X'}, {'-', 'X', '-'}, {'X', '-', '-'}};
        char CurrenPlayer = 'X';
        boolean result = Lab03UnitTest.checkWin(board, CurrenPlayer);
        assertEquals(true, result);
    }
    @Test
    public void testCheckDraw_output_true() {
        char[][] board = {{'X', 'O', 'O'}, {'O', 'X', 'X'}, {'-', 'X', 'O'}};
        char CurrenPlayer = 'X';
        boolean result = Lab03UnitTest.checkDraw(board, CurrenPlayer);
        assertEquals(true, result);
    }

}
